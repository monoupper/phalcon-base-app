<?php
use Phalcon\Di\Service;

/**
 * Class Sender
 * @property \Phalcon\Ext\Mailer\Manager mailer
 */
class MailSender extends \Phalcon\Mvc\User\Component
{
    public function sendMail($subject, $to, $viewPath, $params = [])
    {
        $message = $this->mailer->createMessageFromView($viewPath, $params)
            ->to($to)
            ->subject($subject);

        $message->send();
    }
}