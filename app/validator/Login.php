<?php

use Phalcon\Validation;

class Login extends Validation
{
    /**
     * @var array
     */
    private $error;

    /**
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param null $data
     * @param null $entity
     * @return bool
     */
    public function validate($data = null, $entity = null)
    {
        /**
         * @var Users $user
         */
        $user = Users::findFirst(
            [
                'email = :email:',
                'bind' => [
                    'email' => $data['email'],
                ]
            ]
        );

        if ($user !== false) {
            $password = $data['password'];

            if ($this->security->checkHash($password, $user->getPassword()) === true) {
                $this->session->set('user_identity',[
                    'id'   => $user->getId(),
                    'name' => $user->getName()
                ]);
               return true;
            }

            $this->error = [
                'field' => 'password',
                'message' =>'Password is incorrect. Try again'
            ];

        } else {
            $this->error = [
                'field' => 'email',
                'message' => 'User is not found'
            ];
        }
        return false;
    }
}