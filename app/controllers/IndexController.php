<?php

use Phalcon\Mvc\Controller;

class IndexController extends Controller
{
    public function initialize()
    {
        $this->tag->setTitle('<[{IndexController.php.application_name}]>');
        $this->tag->setTitleSeparator(' - ');
    }

    public function indexAction()
    {

    }
}